import psycopg2
import numpy as np
from flask import Flask, request, jsonify, render_template

app = Flask(__name__)

conn = psycopg2.connect(
    dbname="WebGame",
    user="postgres",
    password="bondfct920213",
    host="localhost",
    port="5432"
)
cursor = conn.cursor()

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload():
    data = request.form.get('data')
    dataList = list(data.split(" +_*/+-"))
    name = dataList[0]
    second = float(dataList[1])
    print(name)
    print(second)

    sql = "INSERT INTO rank (name, second) VALUES (%s, %s)"
    inputData = (name, second)

    cursor.execute(sql, inputData)
    conn.commit()

    return jsonify(message="成績已記錄至資料庫")

@app.route('/rank', methods=['GET'])
def rank():
    sql = "SELECT * FROM public.rank ORDER BY second ASC LIMIT 10"
    cursor.execute(sql)
    rank_results = cursor.fetchall()
    conn.commit()
    for i in range(len(rank_results)):
        rank_results[i] = list(np.insert(rank_results[i], 0, i+1))


    return jsonify(rankChart = rank_results)

if __name__ == '__main__':
    app.run()